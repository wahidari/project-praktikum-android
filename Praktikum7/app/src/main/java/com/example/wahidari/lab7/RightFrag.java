package com.example.wahidari.lab7;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RightFrag extends Fragment {
    TextView changeableText;

    public RightFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        Button doNestingButton = (Button) root.findViewById(R.id.right_frag_button);
        changeableText = (TextView) root.findViewById(R.id.right_frag_text);
        doNestingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment videoFragment = new NestedFrag();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.add(getId(), videoFragment, "right").commit();
            }
        });
        return root;
    }

    public void setChangeableText(String newText) {
        if (changeableText != null)
            changeableText.setText(newText);
    }

}
