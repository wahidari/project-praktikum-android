package com.example.wahidari.lab3;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView t1,t2,t3,t4,t5,t6;
    String name,reg,dept,place,hobby,date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        t1= (TextView) findViewById(R.id.textView1);
        t2= (TextView) findViewById(R.id.textView2);
        t3= (TextView) findViewById(R.id.textView3);
        t4= (TextView) findViewById(R.id.textView4);
        t5= (TextView) findViewById(R.id.textView5);
        t6= (TextView) findViewById(R.id.textView6);

        //Getting the Intent
        Intent i = getIntent();

        //Getting the Values from First Activity using the Intent received
        name =i.getStringExtra("name_key");
        reg  =i.getStringExtra("reg_key");
        dept =i.getStringExtra("dept_key");
        place=i.getStringExtra("place_key");
        hobby=i.getStringExtra("hobby_key");
        date =i.getStringExtra("date_key");

        //Setting the Values to Intent
        t1.setText(name);
        t2.setText(reg);
        t3.setText(dept);
        t4.setText(place);
        t5.setText(hobby);
        t6.setText(date);

    }

}