package com.example.wahidari.lab3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText e1,e2,e3,e4;
    Button bt;
    Spinner s;

    //Data for populating in Spinner
    String [] dept_array={"CSE","ECE","IT","Mech","Civil"};
    String name,reg,dept,place,hobby,date;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TextView tvDateResult;
    private ImageButton btDatePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Referring the Views
        e1 = findViewById(R.id.editText);
        e2 = findViewById(R.id.editText2);
        e3 = findViewById(R.id.editText3);
        e4 = findViewById(R.id.editText4);
        bt = findViewById(R.id.button);
        s  = findViewById(R.id.spinner);

        //Creating Adapter for Spinner for adapting the data from array to Spinner
        ArrayAdapter adapter= new ArrayAdapter(MainActivity.this,android.R.layout.simple_spinner_item,dept_array);
        s.setAdapter(adapter);


        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tvDateResult = findViewById(R.id.textView7);
        btDatePicker = findViewById(R.id.bt_datepicker);
        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        //Creating Listener for Button
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Getting the Values from Views(Edittext & Spinner)
                if (!e1.getText().toString().toString().isEmpty() && !e2.getText().toString().toString().isEmpty() && !e3.getText().toString().toString().isEmpty() && !e4.getText().toString().toString().isEmpty() && !tvDateResult.getText().toString().toString().isEmpty()){
                    name  = e1.getText().toString();
                    reg   = e2.getText().toString();
                    dept  = s.getSelectedItem().toString();
                    place = e3.getText().toString();
                    hobby = e4.getText().toString();
                    date  = tvDateResult.getText().toString();

                    //Intent For Navigating to Second Activity
                    Intent i = new Intent(MainActivity.this,SecondActivity.class);

                    //For Passing the Values to Second Activity
                    i.putExtra("name_key" , name);
                    i.putExtra("reg_key"  , reg);
                    i.putExtra("dept_key" , dept);
                    i.putExtra("place_key", place);
                    i.putExtra("hobby_key", hobby);
                    i.putExtra("date_key" , date);
                    startActivity(i);
                }
                else {
                    Toast.makeText(getApplicationContext(), "All Field Can't Empty", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void showDateDialog() {

        /**
         * Calendar untuk mendapatkan tanggal sekarang
         */
        Calendar newCalendar = Calendar.getInstance();

        /**
         * Initiate DatePicker dialog
         */
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tvDateResult.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        datePickerDialog.show();
    }

}
