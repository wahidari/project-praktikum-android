package com.example.wahidari.acclab5_6;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FragmentTwo extends Fragment {

    public FragmentTwo() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_02, container, false);
        ListView listview = rootView.findViewById(R.id.listView);

        // Get list Name From MainAct
        MainActivity main = new MainActivity();
        String[] list = main.listVal;

        // Set List Name into ListView in Frag 02
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        return rootView;
    }

}
