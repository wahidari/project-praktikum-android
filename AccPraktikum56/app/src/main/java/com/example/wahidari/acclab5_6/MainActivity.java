package com.example.wahidari.acclab5_6;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FragmentThree.UserNameListener {

    ListView listView;
    // Define string array.
    public static String[] listVal = new String[] {"Adi","Toni"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Add New Value to Array Name
    @Override
    public void onFinishUserDialog(String user) {
        // Convert array to list
        List<String> listFromArray = Arrays.asList(listVal);

        // Create new list, because, List to Array always returns a fixed-size list backed by the specified array.
        List<String> tempList = new ArrayList<>(listFromArray);
        tempList.add(user);

        //Convert list back to array
        String[] tempArray = new String[tempList.size()];
        listVal = tempList.toArray(tempArray);
        Toast.makeText(this, user + " Berhasil Ditambahkan" , Toast.LENGTH_SHORT).show();
    }

    public void onButtonClick(View v) {
        // To Check The Device that opened the App (Phone / Tablet)
        Fragment fragmentTwo = getFragmentManager().findFragmentById(R.id.fragment_two);

        // close existing dialog fragments
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag("fragment_edit_name");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }

        switch(v.getId()) {
            case R.id.btn_one_create_new:

                if (fragmentTwo == null) { // PHONE!
                    // Need to launch another activity
                    FragmentThree editNameDialog = new FragmentThree();
                    editNameDialog.show(manager, "fragment_edit_name");
                }

                else {			          // TABLET!
                    FragmentThree editNameDialog = new FragmentThree();
                    editNameDialog.show(manager, "fragment_edit_name");
                }

                break;

            case R.id.btn_one_open_existing:

                if (fragmentTwo == null) {	// PHONE!
                    // Need to launch another activity
                    Intent i = new Intent(this, DrawingActivity.class);
                    startActivity(i);
                }

                else {				       // TABLET!
                    // Do nothing right now, and just show up a toast.
                    Toast.makeText(this, "Silahkan Lihat List Nama", Toast.LENGTH_SHORT).show();
                }

                break;

            default:
        }
    }
}


