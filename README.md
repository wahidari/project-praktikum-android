# Project Praktikum Pemrograman Perangkat Bergerak (Android) 2018

[![](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://wahidari.gitlab.io)
[![](https://semaphoreci.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/400484/shields_badge.svg)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/docs-latest-brightgreen.svg?style=flat&maxAge=86400)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/Find%20Me-%40wahidari-009688.svg?style=social)](https://wahidari.gitlab.io)

## Language

- [![](https://img.shields.io/badge/java-8-red.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/xml-1.0-green.svg)](https://wahidari.gitlab.io) 

## Screenshot

- ##### Praktikum 1

    ![](./Praktikum1/ss/a.png)
    ![](./Praktikum1/ss/b.png)
    
    
- ##### Praktikum 2

    ![](./Praktikum2/ss/a.png)
    
    ![](./Praktikum2/ss/b.png)
    
    
- ##### Praktikum 3

    ![](./Praktikum3/ss/a.png)
    
    ![](./Praktikum3/ss/b.png)
    
    ![](./Praktikum3/ss/c.png)
    
    
- ##### Praktikum 4

    ![](./Praktikum4/ss/a.png)
    
    
- ##### Praktikum 5

    ![](./Praktikum5/ss/a.png)
    
    ![](./Praktikum5/ss/b.png)
    
    ![](./Praktikum5/ss/c.png)
    
    
- ##### Praktikum 6

    ![](./AccPraktikum56/ss/a.png)
    
    ![](./AccPraktikum56/ss/b.png)
    
    ![](./AccPraktikum56/ss/c.png)
    
    ![](./AccPraktikum56/ss/d.png)
    
    
- ##### Praktikum 7

    ![](./Praktikum7/ss/a.jpg)
    
    ![](./Praktikum7/ss/b.jpg)
    
    ![](./Praktikum7/ss/c.jpg)
    
    ![](./Praktikum7/ss/d.jpg)
    
    ![](./Praktikum7/ss/e.jpg)
    
    
- ##### Praktikum 8

    ![](./Praktikum8/ss/a.jpg)
    
    ![](./Praktikum8/ss/b.jpg)
    
## License
> This program is Free Software: 
You can use, study, share and improve it at your will. 
Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.